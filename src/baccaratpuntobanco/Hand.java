package baccaratpuntobanco;

/**
 *
 * @author Andrea
 */
public class Hand extends CardCollection {
    @Override
    public String toString() {
        String s = "";
        //This is a for each loop, it iterates through every Card c contained in the List of cards
        //that I added using "Hand.add(Card c)" method.
        for(Card c:super.cards) {
            s += c.toString() + " "; //For every card, I get the string value (e.g. 4D) and append a whitespace after.
        }
        return s; //This will contain something like 4D 5S JS... (depends on how many cards you add)
    }
    
    public int value() {
        int v = 0;
        for (Card c:super.cards) {
            //Since I know for sure that every card I add is a BaccaratCard object, I can do this casting without worries.
            //However, if needed, you could always check if the "Card" (c in this foreach loop) object is actually a "BaccaratCard" one by using instanceof
            //This would be something like if (c instanceof BaccaratCard) { //Cast and do stuff } else { //This is not actually a BaccaratCard! }
            v += ((BaccaratCard) c).value(); 
        }
        return v % 10;
    }
}
