package baccaratpuntobanco;

import java.util.Scanner;

/**
 *
 * @author Andrea
 */


public class Game {
    
    Shoe shoe = null;
    Hand player = null, banker = null;
    public Game(int numDecks) {
        this.shoe = new Shoe(numDecks); //The use of "this" isn't necessary, but I think it helps reading the code
        this.player = new Hand();
        this.banker = new Hand();
        this.shoe.shuffle();
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numDecks;
        do {
            System.out.println("Please type in the number of decks used in this game: ");
            numDecks = sc.nextInt();
        } while (numDecks != 4 && numDecks != 6 && numDecks != 8);
        Game g = new Game(numDecks);
        g.playRound();
    }
    
    public void playRound() {
        BaccaratCard b1Player, b2Player, b1Banker, b2Banker;
        int numOfPlayerCards = 2;
        //I'll just deal 4 cards: 2 for the player and two for the banker. Then I'll add them to the correct hands.
        b1Player = (BaccaratCard) shoe.deal();
        b2Player = (BaccaratCard) shoe.deal();
        b1Banker = (BaccaratCard) shoe.deal();
        b2Banker = (BaccaratCard) shoe.deal();
        
        player.add(b1Player);
        player.add(b2Player);
        banker.add(b1Banker);
        banker.add(b2Banker);
        
        int playerHandValue = player.value();
        int bankerHandValue = banker.value();
        System.out.println("Player's hand value: " + playerHandValue);
        System.out.println("Banker's hand value: " + bankerHandValue);
        
        //Check for natural, and if so, show the result of the game (who won or if it's a tie).
        if (playerHandValue >= 8 || bankerHandValue >= 8) {
            if (playerHandValue > bankerHandValue)
                System.out.println("Player won!");
            else if(bankerHandValue > playerHandValue)
                System.out.println("Banker won!");
            else
                System.out.println("It's a tie!");
            return;
        }
        //Applying player's rule as described on Wikipedia.
        int playerThirdCardValue = 0; //This will help with the banker's complex rule
        if (playerHandValue <= 5) { //I can deal another card and add it to player's hand.
            BaccaratCard b3Player = (BaccaratCard) shoe.deal();
            player.add(b3Player);
            playerHandValue = player.value();
            System.out.println("New player's hand value: " + playerHandValue);
            numOfPlayerCards++; //This will help me with banker's rule
            playerThirdCardValue = b3Player.value();
        }
        if (numOfPlayerCards == 2) {
            if (bankerHandValue <= 5) {
                BaccaratCard b3Banker = (BaccaratCard) shoe.deal();
                banker.add(b3Banker);
                bankerHandValue = banker.value();
                System.out.println("New banker's hand value: " + bankerHandValue);
            }
        }
        else { //More complex rules are done here!   
            /*
            
            If the banker total is 2 or less, then the banker draws a card, regardless of what the player's third card is.
            If the banker total is 3, then the bank draws a third card unless the player's third card was an 8.
            If the banker total is 4, then the bank draws a third card if the player's third card was 2, 3, 4, 5, 6, 7.
            If the banker total is 5, then the bank draws a third card if the player's third card was 4, 5, 6, or 7.
            If the banker total is 6, then the bank draws a third card if the player's third card was a 6 or 7.
            If the banker total is 7, then the banker stands.

            */
            BaccaratCard b3Banker = (BaccaratCard) shoe.deal();
            switch (bankerHandValue) {
                case 0:
                case 1:
                case 2:
                    banker.add(b3Banker);
                    bankerHandValue = banker.value();
                    System.out.println("New banker's hand value: " + bankerHandValue);
                    break;
                case 3:
                    if (playerThirdCardValue == 8) {
                        banker.add(b3Banker);
                        bankerHandValue = banker.value();
                        System.out.println("New banker's hand value: " + bankerHandValue);
                    }
                    break;
                case 4:
                    if (playerThirdCardValue >= 2 && playerThirdCardValue <= 7) {
                        banker.add(b3Banker);
                        bankerHandValue = banker.value();
                        System.out.println("New banker's hand value: " + bankerHandValue);
                    }
                    break;
                case 5:
                    if (playerThirdCardValue >= 4 && playerThirdCardValue <= 7) {
                        banker.add(b3Banker);
                        bankerHandValue = banker.value();
                        System.out.println("New banker's hand value: " + bankerHandValue);
                    }
                    break;
                case 6:
                    if (playerThirdCardValue == 6 || playerThirdCardValue == 7) {
                        banker.add(b3Banker);
                        bankerHandValue = banker.value();
                        System.out.println("New banker's hand value: " + bankerHandValue);
                    }
                    break;
            }
        }
        
        if (playerHandValue > bankerHandValue)
            System.out.println("Player won!");
        else if (bankerHandValue > playerHandValue)
            System.out.println("Banker won!");
        else
            System.out.println("égalité — tie bets win");
    }
}
