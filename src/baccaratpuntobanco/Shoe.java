package baccaratpuntobanco;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Andrea
 */
public class Shoe extends CardCollection {
    public Shoe (int numDecks) {
        //I'll check for the correct number of decks in the Game class.
        List<Character> ranks = Card.getRanks(); //I get all the ranks
        List<Character> suits = Card.getSuits(); //And all the suits. Then
        for (int i = 0; i < numDecks; i++) { //For each deck
            for (int k = 0; k < ranks.size(); k++) { //For every rank
                for (int l = 0; l < suits.size(); l++) { //I change the suit, so that I add cards in this order: A C, A D, A H, A S, 2 C, 2 D, 2 H, 2 S, ...
                    cards.add(new BaccaratCard(ranks.get(k), suits.get(l)));
                }
            }
        }
    }
    
    public void shuffle() {
        Random r = new Random((long) (Math.random() * Long.MAX_VALUE)); //This way I generate a Random instance with a new seed everytime. (More random)
        Collections.shuffle(cards, r);
    }
    
    public Card deal() {
        int topCardIndex = cards.size() - 1; //The top card is at the position "cards.size() - 1" (If size is 52, cards indexes are from 0 to 51!)
        Card toDeal = cards.get(topCardIndex); //So I just get it and store it
        cards.remove(topCardIndex); //Then I remove it from the shoe
        return toDeal; // And return it
    }
}
