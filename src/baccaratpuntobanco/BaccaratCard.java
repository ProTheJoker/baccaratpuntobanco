package baccaratpuntobanco;

/**
 *
 * @author Andrea
 */
public class BaccaratCard extends Card {
    public BaccaratCard (char rank, char suit) {
        super(rank, suit);
    }
    
    public BaccaratCard (String code) {
        super(code);
    }
    
    @Override
    public int value() {
        //Doing value % 10 makes the unit stay, since the % is the remainder between the value and 10.
        //This means that 15 % 10 is the remainder, so 15:10 = 1 with remainder of 5, and that is the card's value in Baccarat.
        return super.value() % 10;
    }
}
